# Datastructures Helper Lib #

## Include in Node ##

```javascript
import { Task, Queue, Graph, Element, Connection, Node, Tree, List } from "@stormking/datastructures";
// or
const { Task, Queue, Graph, Element, Connection, Node, Tree, List } = require("@stormking/datastructures");
```

## Include in Browser ##

```javascript
import Tree from '@stormking/datastructures/dist/browser/tree.js'
import Queue from '@stormking/datastructures/dist/browser/queue.js'
import Tasks from '@stormking/datastructures/dist/browser/tasks.js'
import Lists from '@stormking/datastructures/dist/browser/lists.js'
```

## Graph ##

An in-memory graph-db, trying to fill the role that NeDB fills for document databases.

```javascript
let g = new Graph();
let a = g.createNode();
let b = g.createNode();
a.linkTo(b);
```

See [tests](https://gitlab.com/stormking/datastructures/-/blob/master/test/unit/graph.test.ts) for further usage.

## Queue ##

A simple queue, implemented via dual-linked list for optimal performance.

```javascript
let q = new Queue<number>();
q.push(5);
let x = q.shift();
q.unshift(x);
x = q.pop();
```

See [tests](https://gitlab.com/stormking/datastructures/-/blob/master/test/unit/queue.test.ts) for further usage.

## Task ##

A task queue.

```javascript
let q = new Task();
let result = await q.addTask(() => Date.now());
```

See [tests](https://gitlab.com/stormking/datastructures/-/blob/master/test/unit/task.test.ts) for further usage.
