
export class Element<T> {

	public data: T;
	public next: Element<T>;
	public prev: Element<T>;

	constructor(data: T) {
		this.data = data;
	}
}

export default class Queue<T> {

	protected first: Element<T>;
	protected last: Element<T>;
	protected size = 0;

	push(d: T) {
		this.size += 1;
		let e = new Element(d);
		if (this.last) {
			this.last.next = e;
			e.prev = this.last;
		} else {
			this.first = e;
		}
		this.last = e;

	}

	pop(): T|null {
		if (!this.last) return null;
		let e = this.last;
		this.last = e.prev;
		if (this.last) this.last.next = null;
		else this.first = null;
		e.prev = null;
		this.size -= 1;
		return e.data;
	}

	unshift(d: T) {
		this.size += 1;
		let e = new Element(d);
		if (this.first) {
			this.first.prev = e;
			e.next = this.first;
		} else {
			this.last = e;
		}
		this.first = e;
	}

	shift(): T|null {
		if (!this.first) return null;
		let e = this.first;
		this.first = e.next;
		if (this.first) this.first.prev = null;
		else this.last = null;
		e.next = null;
		this.size -= 1;
		return e.data;
	}

	getSize(): number {
		return this.size;
	}

	*[Symbol.iterator](): IterableIterator<T> {
		let current = this.first;
		while(current) {
			yield current.data;
			current = current.next;
		}
	}
	clear() {
		this.first = null;
		this.last = null;
		this.size = 0;
	}

}
