import Queue from './queue';

class TaskJob {
	static counter = 0;
	public task: Function;
	public resolve: Function;
	public reject: Function;
	public n: number;
	constructor() {
		this.n = ++TaskJob.counter;
	}
}

export default class TaskQueue {

	protected parallel = 1;
	protected running = 0;
	protected waiting = 0;
	protected q: Queue<TaskJob>

	constructor(p: number = 1) {
		this.parallel = p;
		this.q = new Queue();
	}

	addTask(fn: Function): Promise<any> {
		let job = new TaskJob();
		job.task = fn;
		let p = new Promise((res, rej) => {
			job.resolve = res;
			job.reject = rej;
		});
		this.q.push(job);
		this.waiting += 1;
		this.runNext();
		return p;
	}

	canAddJob() {
		return this.waiting > 0
			&& this.running < this.parallel;
	}

	runNext() {
		if (!this.canAddJob()) return;
		let job = this.q.shift();
		this.waiting -= 1;
		this.running += 1;
		Promise.resolve()
			.then(() => job.task())
			.catch(e => {
				job.reject(e);
				this.runNext();
			})
			.then(r => {
				this.running -= 1;
				job.resolve(r);
				this.runNext();
			});
		this.runNext();
	}

	waitForDrain() {
		if (this.waiting === 0 && this.running === 0) return Promise.resolve();
		return this.addTask(() => {
			if (this.waiting === 0) {
				return true;
			} else {
				return false;
			}
		}).then(done => {
			if (done) return;
			else return this.waitForDrain();
		})
	}

	getWaiting() {
		return this.waiting;
	}

}
