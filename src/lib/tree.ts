import List from './list';

type Child<T> = {
	name: string,
	child: TreeNode<T>
}

export default class TreeNode<T> {

	protected parent: TreeNode<T>;
	protected content: T;
	protected children: List<Child<T>>;
	protected name: string;

	constructor(name: string, content: T, parent?: TreeNode<T>) {
		this.name = name;
		this.parent = parent;
		this.content = content;
		this.children = new List<Child<T>>();
	}

	setContent(c: T) {
		this.content = c;
	}

	*iterateOverParents() {
		let node: TreeNode<T> = this;
		while(node) {
			yield node;
			node = node.getParent();
		}
	}

	addChild(name: string, content: T): TreeNode<T> {
		let child = new TreeNode(name, content, this);
		this.children.push({ name, child });
		return child;
	}

	removeChild(name: string) {
		this.children.removeOne((c) => c.name === name);
	}

	removeChildren() {
		this.children.clear();
	}

	findChild(matcher: (T, string) => boolean, deep?: boolean): TreeNode<T> {
		let iterator = deep ? this.iterateOverAllChildren() : this.iterateOverChildren();
		for (let c of iterator) {
			let match = matcher(c.getContent(), c.getName());
			if (match) return c;
		}
		return null;
	}

	hasChildren(): boolean {
		return this.children.getSize() > 0;
	}

	getChildCount(): number {
		return this.children.getSize();
	}

	getChild(name: string): TreeNode<T> {
		let child = this.children.find(c => c.name === name);
		return child ? child.child : null;
	}

	getName(): string {
		return this.name;
	}

	getPath(): string[] {
		let p = [];
		for (let n of this.iterateOverParents()) {
			if (!n.getParent() && !n.getName()) continue;
			p.push(n.getName());
		}
		return p.reverse();
	}

	getChildFromPath(p: string[]): TreeNode<T>|null {
		if (p.length === 0) return this;
		let [name, ...r] = p;
		let c = this.getChild(name);
		if (!c) return null;
		return c.getChildFromPath(r);
	}

	getFullChildCount(): number {
		let n = 0;
		for (let c of this.iterateOverAllChildren()) {
			n += 1;
		}
		return n;
	}

	*iterateOverChildren(): IterableIterator<TreeNode<T>> {
		for (let { child } of this.children) {
			yield child;
		}
	}

	*iterateOverAllChildren() {
		for (let { child } of this.children) {
			yield child;
			if (child.hasChildren()) {
				for (let s of child.iterateOverAllChildren()) {
					yield s;
				}
			}
		}
	}

	*[Symbol.iterator](): IterableIterator<TreeNode<T>> {
		for (let { child } of this.children) {
			yield child;
		}
	}

	getParent(): TreeNode<T>|null {
		return this.parent;
	}

	getContent(): T {
		return this.content;
	}

	getChildrenSorted(sorter: (a: { name: string, content: T }, b: {name: string, content: T}) => number): TreeNode<T>[] {
		let arr = Array.from(this.iterateOverChildren());
		arr.sort((a: TreeNode<T>, b: TreeNode<T>) => {
			return sorter(
				{ name: a.getName(), content: a.getContent() },
				{ name: b.getName(), content: b.getContent() }
			);
		});
		return arr;
	}

	toJSON() {
		let content = {};
		for (let [k, v] of Object.entries(this.content)) {
			if (typeof k === 'string' || 'toString' in k) {
				content[''+k] = v;
			}
		}
		let children = Array.from(this.iterateOverChildren());
		return {
			content,
			children
		};
	}

}
