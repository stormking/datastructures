import Queue, { Element } from './queue';

type Matcher = (T) => boolean;

export default class List<T> extends Queue<T> {

	find(matcher: Matcher): T|null {
		for (let e of this) {
			let m = matcher(e);
			if (m) return e;
		}
		return null;
	}

	removeOne(matcher: Matcher) {
		this.removeInternal(matcher, true);
	}

	removeAny(matcher: Matcher) {
		this.removeInternal(matcher, false);
	}

	protected removeInternal(matcher: Matcher, stop: boolean) {
		let current = this.first;
		while(current) {
			let m = matcher(current.data);
			if (m) {
				this.removeElement(current);
				if (stop) return;
			}
		}
	}

	protected removeElement(e: Element<T>) {
		let prev = e.prev;
		let next = e.next;
		e.prev = null;
		e.next = null;
		if (prev) {
			prev.next = next;
		}
		if (next) {
			next.prev = prev;
		}
		this.size -= 1;
	}

}
