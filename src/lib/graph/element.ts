import Graph from "./graph";


export default class Element {

	static counter = 0;

	protected id: string;
	protected rev: number;
	protected readonly attributes: Map<any, any> = new Map();
	protected readonly net: Graph;
	protected destroyed = false;

	constructor(net: Graph, id?: string) {
		this.id = id || 'e'+ (++Element.counter);
		this.rev = 0;
		this.net = net;
	}

	addData(d: Map<any,any>|Object) {
		if (!d) return;
		if (d instanceof Map) {
			for (let [k, v] of d.entries()) {
				this.attributes.set(k, v);
			}
		} else {
			Object.entries(d).forEach(([k, v]) => {
				this.attributes.set(k, v);
			})
		}
	}

	setAttribute(key: any, val: any) {
		this.attributes.set(key, val);
		this.net.updateElement(this);
	}

	getAttributesClone() {
		let map = new Map(this.attributes);
		return map;
	}

	getAttribute(key: any) {
		return this.attributes.get(key);
	}

	deleteAttribute(key: any) {
		let r = this.attributes.delete(key);
		this.net.updateElement(this);
		return r;
	}

	hasAttribute(key: any) {
		return this.attributes.has(key);
	}

	clearAttributes() {
		this.attributes.clear();
		this.net.updateElement(this);
	}

	destroy() {
		this.destroyed = true;
		this.clearAttributes();
		this.net.removeElement(this);
	}

	isDestroyed() {
		return this.destroyed;
	}

	getId() {
		return this.id;
	}

	getRev() {
		return this.rev;
	}

	setRev(r: number) {
		this.rev = r;
	}

}
