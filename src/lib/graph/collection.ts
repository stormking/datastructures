import Element from './element';
import Node from './node';
import Connection from './connection';

export type C = NodeCollection | ConnectionCollection;

abstract class Collection {

	protected readonly data: Set<Node | Connection>;
	protected readonly parent: C;

	constructor(parent?: C) {
		this.parent = parent;
	}

	getParent(): C {
		return this.parent;
	}

	*[Symbol.iterator](): IterableIterator<Node | Connection> {
		for (let e of this.data) {
			if (!e.isDestroyed()) {
				yield e;
			} else {
				this.remove(e);
			}
		}
	}

	count() {
		return this.data.size;
	}

	clearDestroyed(): number {
		let c = 0;
		for (let e of this.data) {
			if (e.isDestroyed()) {
				this.remove(e);
				c += 1;
			}
		}
		return c;
	}

	add(e: Node | Connection) {
		this.data.add(e);
	}

	remove(e: Node | Connection) {
		this.data.delete(e);
	}

	clear() {
		this.data.clear();
	}

	getFirst(): Node | Connection {
		for (let e of this.data) {
			if (!e.isDestroyed()) {
				return e;
			} else {
				this.remove(e);
			}
		}
		return null;
	}

}

export class NodeCollection extends Collection {

	protected readonly data: Set<Node>;

	constructor(set: Set<Node>, parent?: C) {
		super(parent);
		this.data = set;
	}

	getConnectionsMatching(from=true, to=true, filter?: Function) {
		let res = new Set<Connection>();
		for (let elem of this.data) {
			for (let conn of elem.getLinks()) {
				if ((	(from && conn.getFrom() === elem)
					||	(to && conn.getTo() === elem))
					&&	(!filter || filter(elem))) {
					res.add(conn);
				}
			}
		}
		return new ConnectionCollection(res, this);
	}

	merge(other: NodeCollection | Set<Node>): NodeCollection {
		let data = new Set(this.data);
		for (let e of other) {
			data.add(e);
		}
		return new NodeCollection(data, this);
	}

	filter(filter: Function): NodeCollection {
		let res = new Set<Node>();
		for (let elem of this.data) {
			if (!filter || filter(elem)) {
				res.add(elem);
			}
		}
		return new NodeCollection(res, this);
	}

	*[Symbol.iterator](): IterableIterator<Node> {
		for (let e of super[Symbol.iterator]()) {
			yield <Node>e;
		}
	}

	getFirst(): Node {
		return <Node>super.getFirst();
	}

	gatherNodesWithinHops(hops=1, filter=null, excludeCurrent=false, stopAtFiltered=false) {
		let res = new Set<Node>();
		const add = (n: Node, level: number) => {
			if (res.has(n)) return;
			let filterSuccess = !filter || filter(n);
			if (!filterSuccess && stopAtFiltered) return;
			if (!res.has(n) && (!excludeCurrent || !this.data.has(n)) && filterSuccess) {
				res.add(n);
			}
			level -= 1;
			if (level === 0) return;
			for (let l of n.getLinks()) {
				if (l.getFrom() !== n) add(l.getFrom(), level);
				if (l.getTo() !== n) add(l.getTo(), level);
			}
		}
		for (let n of this.data) {
			add(n, hops+1);
		}
		return new NodeCollection(res, this);
	}

}

export class ConnectionCollection extends Collection {

	protected readonly data: Set<Connection>;

	constructor(set: Set<Connection>, parent?: C) {
		super(parent);
		this.data = set;
	}

	getNodesFromConnection(from=true, to=true, filter?: Function) {
		let res = new Set<Node>();
		for (let elem of this.data) {
			if (from && (!filter || filter(elem))) {
				res.add(elem.getFrom());
			}
			if (to && (!filter || filter(elem))) {
				res.add(elem.getTo());
			}
		}
		return new NodeCollection(res, this);
	}

	merge(other: ConnectionCollection | Set<Connection>): ConnectionCollection {
		let data = new Set(this.data);
		for (let e of other) {
			data.add(e);
		}
		return new ConnectionCollection(data, this);
	}

	filter(filter: Function): ConnectionCollection {
		let res = new Set<Connection>();
		for (let elem of this.data) {
			if (!filter || filter(elem)) {
				res.add(elem);
			}
		}
		return new ConnectionCollection(res, this);
	}

	*[Symbol.iterator](): IterableIterator<Connection> {
		for (let e of super[Symbol.iterator]()) {
			yield <Connection>e;
		}
	}

	getFirst(): Connection {
		return <Connection>super.getFirst();
	}

}
