import Storage from "./storage";
import Element from "./element";
import * as fs from 'fs';

export default class StorageAutosave extends Storage {

	protected queuedWrites = new Set<Element>();
	protected queue: number;
	protected queueTime = 1000;
	protected writeStream: fs.WriteStream;
	protected status: number = 0;
	static STATUS_IDLE = 0;
	static STATUS_QUEUED = 1;
	static STATUS_WRITING = 2;

	async load(file: string) {
		await super.load(file);
		let write = fs.createWriteStream(file, { encoding: 'utf8' });
		this.setTarget(write);
	}

	setAutosaveTimer(ms: number) {
		if (ms < 0) ms = 0;
		this.queueTime = ms;
	}

	protected queueTimer() {
		if (this.status !== StorageAutosave.STATUS_IDLE) return;
		if (!this.writeStream || !this.writeStream.writableEnded) return;
		this.status = StorageAutosave.STATUS_QUEUED;
		this.queue = <any>setTimeout(() => {
			this.doWriteUpdate();
		}, this.queueTime);
	}

	addElement(e: Element) {
		super.addElement(e);
		this.queuedWrites.add(e);
		this.queueTimer();
	}

	updateElement(e: Element) {
		super.updateElement(e);
		this.queuedWrites.add(e);
		this.queueTimer();
	}

	removeElement(e: Element) {
		super.removeElement(e);
		this.queuedWrites.add(e);
		this.queueTimer();
	}

	async doWriteUpdate() {
		if (this.status === StorageAutosave.STATUS_WRITING) return;
		if (!this.writeStream || !this.writeStream.writableEnded) {
			throw new Error('writeStream not defined or closed');
		}
		this.status = StorageAutosave.STATUS_WRITING;
		this.queue = null;
		let set = this.queuedWrites;
		this.queuedWrites = new Set();

		await this.saveToFile(this.writeStream, set);

		this.status = StorageAutosave.STATUS_IDLE;
		if (this.queuedWrites.size > 0) {
			this.queueTimer();
		}
	}

	protected cleanupFile() {

	}

	setTarget(write: fs.WriteStream) {
		this.writeStream = write;
	}

}
