import Element from "./element";
import Node from "./node";
import Graph from "./graph";


export default class Connection extends Element {

	protected from: Node;
	protected to: Node;

	constructor(net: Graph, f: Node, t: Node, id?: string) {
		super(net, id);
		this.from = f;
		this.to = t;
	}

	getFrom() {
		return this.from;
	}

	getTo() {
		return this.to;
	}

	destroy() {
		this.from = null;
		this.to = null;
		super.destroy();
	}

}
