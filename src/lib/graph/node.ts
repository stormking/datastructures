import Element from "./element";
import Connection from "./connection";
import { ConnectionCollection } from './collection';

export default class Node extends Element {

	protected readonly links: Map<Node, Connection> = new Map();

	linkTo(other: Node, attr?: Map<any,any>, merge=false): Connection {
		if (other === this) throw new Error('cannot link to self');
		if (this.links.has(other)) {
			if (merge) {
				let link = this.links.get(other);
				link.addData(attr);
			} else {
				throw new Error('link already exists')
			}
		} else {
			let link = new Connection(this.net, this, other);
			link.addData(attr);
			this.links.set(other, link);
			other.links.set(this, link);
		}
		return this.links.get(other);
	}

	unlink(other: Node) {
		let link = this.links.get(other);
		if (!link) return;
		this.links.delete(other);
		other.links.delete(this);
		link.destroy();
	}

	getLinks(filter?: Function): ConnectionCollection {
		let set = new Set(this.links.values());
		let c = new ConnectionCollection(set);
		if (filter) return c.filter(filter);
		else return c;
	}

	destroy() {
		for (let [other] of this.links) {
			this.unlink(other);
		}
		super.destroy();
	}


}
