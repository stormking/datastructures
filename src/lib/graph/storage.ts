import * as fs from 'fs';
import * as readline from 'readline';
import * as stream from 'stream';
import * as crypto from 'crypto';
import Debug from 'debug';
const debug = Debug('simple');
import Graph from "./graph";
import Node from './node';
import Element from './element';
import Queue from '../queue';
import Connection from './connection';

function getId() {
	let rnd = crypto.randomBytes(6).toString('hex');
	let now = Date.now().toString(16);
	return Buffer.from(`${now}${rnd}`, 'hex').toString('base64');
}

interface Serialized {
	id?: string,
	rev: number,
	type?: string,
	attributes?: Array<any>,
	from?: string,
	to?: string,
	del?: boolean
}

export default class Storage extends Graph {

	async load(file: string) {
		let read = fs.createReadStream(file, { encoding: 'utf8' });
		await this.loadFromFile(read);
	}

	async loadFromFile(file: stream.Readable) {
		const rl = readline.createInterface({
   			input: file,
 			crlfDelay: Infinity
		});
		let loaded = new Map();
		let stats = {
			nodes: 0,
			edges: 0
		};
		for await(let line of rl) {
			if (line.length === 0) continue;
			let e = this.unserializeElement(line, loaded);
			if (!e) continue;
			if (e instanceof Node) {
				this.all.add(e);
				stats.nodes += 1;
			} else {
				stats.edges += 1;
			}
		}
		return stats;
	}

	saveToFile(file: stream.Writable, from?: Set<Element>) {
		return new Promise((res, rej) => {
			if (!from) from = this.all;
			if (from.size === 0) return res();
			let nodes = new Map<Element, string>();
			function addLinks(e: Node) {
				add(e);
				for (let conn of e.getLinks()) {
					// debug('connection', conn);
					add(conn.getFrom());
					add(conn.getTo());
					add(conn);
				}
			}
			function add(e: Element) {
				if (nodes.has(e)) return false;
				let id = getId();
				nodes.set(e, id);
				// debug('pushing to buffer', e);
				buffer.push(e);
				return true;
			}
			let buffer = new Queue<Element>();
			for (let e of from) {
				if (e instanceof Node) addLinks(e);
				else add(e);
			}
			let that = this;
			let reader = new stream.Readable({
				read() {
					let e = buffer.shift();
					// debug('pulling from buffer', e);
					if (e) {
						let str = that.serializeElement(e, nodes);
						if (e instanceof Node) addLinks(e);
						this.push(str+"\n");
					} else {
						this.push(null);
					}

				}
			});
			reader.pipe(file);
			reader.on('end', () => { setTimeout(res, 10) });
			file.on('error', rej);
		});

	}

	serializeElement(e: Element, ids: Map<Element, string>) {
		let o: Serialized;
		if (e.isDestroyed()) {
			o = {
				del: true,
				id: ids.get(e),
				rev: e.getRev()+1
			};
		} else {
			if (e instanceof Connection) {
				let from = ids.get(e.getFrom());
				let to = ids.get(e.getTo());
				o = {
					type: 'c',
					rev: e.getRev()+1,
					attributes: [],
					from,
					to
				};
			} else {
				o = {
					id: ids.get(e),
					rev: e.getRev()+1,
					type: 'e',
					attributes: []
				};
			}
			for (let row of e.getAttributesClone().entries()) {
				o.attributes.push(row)
			}
		}
		return JSON.stringify(o);
	}

	unserializeElement(str: string, loaded: Map<string, Element>) {
		debug('unserializing', str);
		let o: Serialized = JSON.parse(str);
		let e: Element;
		if (o.id && loaded.has(o.id)) {
			e = loaded.get(o.id);
			if (!e) {
				//has been deleted
				// console.log('skipping deleted load');
				return null;
			}
			if (e.getRev() > o.rev) return e;
			e.clearAttributes();
		}
		if (o.del) {
			e = null;
		} else {
			if (o.type === 'c') {
				if (!loaded.has(o.from)) throw new Error(`from (${o.from}) not loaded`)
				if (!loaded.has(o.to)) throw new Error(`to (${o.to}) not loaded`)
				if (!e) {
					let from =  loaded.get(o.from);
					let to = loaded.get(o.to);
					if (from instanceof Node && to instanceof Node) {
						e = from.linkTo(to);
						debug('loaded link', o.from, o.to);
					} else {
						//from or to deleted
						debug('cannot load connection, from or to deleted');
						return null;
					}
				}
			} else {
				if (!e) {
					e = new Node(this);
				}
			}
			o.attributes.forEach(([key, val]) => {
				e.setAttribute(key, val);
			});
			e.setRev(o.rev);
		}
		loaded.set(o.id, e);
		return e;
	}

}
