import Element from "./element";
import Node from "./node";
import { NodeCollection } from './collection';

export default class Graph {

	protected all = new Set<Node>();

	getAll() {
		return this.all;
	}

	createNode(id?: string) {
		let node = new Node(this, id);
		this.addElement(node);
		return node;
	}

	filter(filter: Function): NodeCollection {
		return this.getCollection(this.all).filter(filter);
	}

	getCollection(elems?: Set<Node>): NodeCollection {
		let collection = new NodeCollection(elems || new Set(this.getAll()));
		return collection;
	}

	addElement(elem: Element) {
		if (elem instanceof Node) {
			this.all.add(elem);
		}
	}

	updateElement(elem: Element) {

	}

	removeElement(elem: Element) {
		if (elem instanceof Node) {
			this.all.delete(elem);
		}
	}

}
