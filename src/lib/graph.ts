import Graph from './graph/storage';
import NodeC from './graph/node';
import ElementC from './graph/element';
import ConnectionC from './graph/connection';
import { C } from './graph/collection';
interface Connection extends ConnectionC {}
interface Node extends NodeC {}
interface Element extends ElementC {}
type Collection = C;
export default Graph;
export {
	Node,
	Element,
	Graph,
	Connection,
	Collection
};
