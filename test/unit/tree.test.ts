import TreeNode from '../../src/lib/tree';
import { expect } from 'chai';

describe('Tree', () => {

	it('can add children', () => {
		let root = new TreeNode<number>('', 1);
		let child = root.addChild('a', 2);
		expect(root.getChild('a')).to.equal(child);
		expect(root.getChildCount()).to.equal(1);
		expect(root.findChild((a: number) => a === 2).getContent()).to.equal(2);
	})

	describe('iteration', () => {
		let tree: TreeNode<number>;
		before(() => {
			tree = new TreeNode<number>('', 1);
			tree.addChild('a', 2);
			tree.addChild('b', 3)
				.addChild('c', 4)
				.addChild('d', 5);
			tree.getChild('b')
				.addChild('e', 6);
		});

		it('can iterate children', () => {
			let r = Array.from(tree.iterateOverChildren());
			expect(r.length).to.equal(2);
			expect(r[0].getContent()).to.equal(2);
			expect(r[1].getContent()).to.equal(3);
		});

		it('can iterate over all children', () => {
			let r = Array.from(tree.iterateOverAllChildren());
			expect(r.length).to.equal(5);
		});

		it('can find deep child', () => {
			let d = tree.findChild((num, name) => num === 5, true);
			expect(d instanceof TreeNode).to.be.true;
			let p = d.getPath().join('/');
			expect(p).to.equal('b/c/d')
		});
	});


});
