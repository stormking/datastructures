import TaskQueue from '../../src/lib/tasks';
import { expect } from 'chai';

describe('Tasks', () => {

	function getWaitTask(ms: number, fail=false) {
		return () => {
			return new Promise((res,rej) => {
				setTimeout(fail ? rej : res, ms);
			});
		}
	}

	describe('basics', () => {

		it('can add a single job', async() => {
			let q = new TaskQueue();
			let t = () => Promise.resolve(2);
			let res = await q.addTask(t)
			expect(res).to.equal(2);
		});

		it('can add multiple at the same time', async() => {
			let q = new TaskQueue();
			let p1 = q.addTask(() => Promise.resolve(1));
			let p2 = q.addTask(() => Promise.resolve(2));
			let p3 = q.addTask(() => Promise.resolve(3));
			let res = await Promise.all([p1, p2, p3]);
			expect(res[0]).to.equal(1);
			expect(res[1]).to.equal(2);
			expect(res[2]).to.equal(3);
		});

		it('can add multiple in sequence', async() => {
			let q = new TaskQueue();
			let r1 = await q.addTask(() => Promise.resolve(1));
			let r2 = await q.addTask(() => Promise.resolve(2));
			let r3 = await q.addTask(() => Promise.resolve(3));
			expect(r1).to.equal(1);
			expect(r2).to.equal(2);
			expect(r3).to.equal(3);
		});

	});

	describe('drain', async() => {
		it('can wait for drain', async() => {
			let q = new TaskQueue();
			let start = Date.now();
			q.addTask(getWaitTask(20));
			q.addTask(getWaitTask(20));
			await q.waitForDrain();
			let end = Date.now();
			expect(end - start).to.be.greaterThan(38);
			expect(q.getWaiting()).to.equal(0);
		});

		it('will re-queue drain', async() => {
			let q = new TaskQueue();
			let start = Date.now();
			q.addTask(getWaitTask(20))
			let p = q.waitForDrain();
			q.addTask(getWaitTask(20))
			await p;
			let end = Date.now();
			expect(end - start).to.be.greaterThan(38);
			expect(q.getWaiting()).to.equal(0);
		});
	});


	describe('parallel', () => {

		it('can do 2 jobs in parallel', async() => {
			let q = new TaskQueue(2);
			let start = Date.now();
			let p1 = q.addTask(getWaitTask(20));
			let p2 = q.addTask(getWaitTask(15));
			await Promise.all([p1, p2]);
			let end = Date.now();
			expect(end - start).to.be.lessThan(35);
		});

		it('can let 3rd job wait', async() => {
			let q = new TaskQueue(2);
			let start = Date.now();
			let p1 = q.addTask(getWaitTask(17));
			let p2 = q.addTask(getWaitTask(16));
			let p3 = q.addTask(getWaitTask(15));
			await Promise.all([p1, p2, p3]);
			let end = Date.now();
			expect(end - start).to.be.greaterThan(20);
		});

	});

	describe('failure', () => {

		it('can fail one job', async() => {
			let q = new TaskQueue();
			let t = () => Promise.reject(new Error('test'));
			let thrown = false;
			try {
				await q.addTask(t)
			} catch(e) {
				thrown = true;
				expect(e.message).to.equal('test')
			}
			expect(thrown).to.be.true;
		});

		it('can continue after failure', async() => {
			let q = new TaskQueue();
			let p1 = q.addTask(getWaitTask(1, true))
				.catch(() => true)
			let p2 = q.addTask(getWaitTask(1, false))
				.then(() => false)
			let p3 = q.addTask(getWaitTask(2, true))
				.catch(() => true)
			let p4 = q.addTask(getWaitTask(2, false))
				.then(() => false)
			let res = await Promise.all([p1, p2, p3, p4]);
			expect(res[0]).to.be.true
			expect(res[3]).to.be.false;
		});

	});

});
