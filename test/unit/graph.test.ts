import { Node, Graph, Connection } from '../../src/lib/graph';
import { expect } from 'chai';
import * as stream from 'stream';
import * as crypto from 'crypto';
import Debug from 'debug';
const debug = Debug('test');

describe('Graph', () => {

	function getTestNet1() {
		let net = new Graph();
		let n1 = net.createNode();
		n1.setAttribute('a', 1);
		let n2 = net.createNode();
		n2.setAttribute('a', 2);
		let c1 = n1.linkTo(n2);
		c1.setAttribute('a', 3);
		return net;
	}

	function getTestNetRandom(nodes: number) {
		let all = new Set<Node>();
		let net = new Graph();
		function getRandomNode(): Node {
			let items = Array.from(all);
    		return items[Math.floor(Math.random() * items.length)];
		}
		function getRandomMap() {
			let m = new Map();
			let k = crypto.randomBytes(5).toString('hex');
			let v = Math.random() * Date.now();
			m.set(k, v);
			return m;
		}
		let initial = net.createNode();
		all.add(initial);
		while (all.size < nodes) {
			let n = net.createNode();
			n.addData(getRandomMap());
			n.setRev(~~(Math.random()*10));
			for (let i=0, ii=~~(Math.random()*3)+1; i<ii; i++) {
				let o = getRandomNode();
				n.linkTo(o, getRandomMap(), true);
			}
			all.add(n);
		}
		return net;
	}

	class TestWritable extends stream.Writable {
		public buffer = '';
		_write(ch, enc, cb) {
			let str = ch.toString('utf8');
			// debug('writing', str);
			this.buffer += str;
			cb();
		}
	}

	class TestReadable extends stream.Readable {
		public buffer = '';
		_read() {
			this.push(this.buffer);
			this.push(null);
		}
	}

	describe('basic functions', () => {

		let graph, n1, n2, n3;
		before(() => {
			graph = new Graph();
		});
		it('can add nodes and link them', () => {
			n1 = graph.createNode();
			n1.addData({ test: true });
			expect(n1.getAttribute('test')).to.be.true;
			n1.deleteAttribute('test');
			expect(n1.getAttribute('test')).to.be.undefined;
			n2 = graph.createNode();
			let c1 = n1.linkTo(n2);
			let links = n1.getLinks();
			expect(links.count()).to.equal(1);
			expect(links.getFirst()).to.equal(c1);
			expect(c1.getTo()).to.equal(n2);
			expect(c1.getFrom()).to.equal(n1);
			links = n2.getLinks();
			expect(links.count()).to.equal(1);
			expect(links.getFirst()).to.equal(c1);
		});

		it('can make collections', () => {
			let c1 = graph.getCollection(new Set());
			c1.add(n1);
			let c2 = c1.getConnectionsMatching();
			expect(c2.getParent()).to.equal(c1);
			expect(c2.count()).to.equal(1);
			let c2b = c2.filter(c => c.getFrom() === n1);
			expect(c2b.getParent()).to.equal(c2);
			expect(c2b.count()).to.equal(1);
			let c3 = c2.getNodesFromConnection();
			expect(c3.count()).to.equal(2);
		});

		it('can merge collections', () => {
			n3 = graph.createNode();
			n1.linkTo(n3);
			let l1 = n3.getLinks();
			let l2 = n2.getLinks();
			let l3 = l1.merge(l2);
			expect(l1.count()).to.equal(1);
			expect(l2.count()).to.equal(1);
			expect(l3.count()).to.equal(2);

			let c3 = graph.getCollection(new Set());
			c3.add(n1);
			let c4 = graph.getCollection(new Set());
			c4.add(n2);
			let c5 = c3.merge(c4);
			let expected = [n1, n2];
			for (let n of c5) {
				expect(expected).to.include(n);
			}
		});

		it('can create collections within hops', () => {
			let m1 = graph.createNode('x1');
			let m2 = graph.createNode('x2');
			let m3 = graph.createNode('x3');
			let m4 = graph.createNode('x4');
			m1.linkTo(m2);
			m2.linkTo(m3);
			m2.linkTo(m4);
			let c4 = graph.getCollection(new Set());
			c4.add(m1);
			let c5 = c4.gatherNodesWithinHops(1);
			expect(c5.count()).to.equal(2);
			let c6 = c4.gatherNodesWithinHops(2);
			expect(c6.count()).to.equal(4);
			let c7 = c5.gatherNodesWithinHops(1, null, true);
			expect(c7.count()).to.equal(2);
			let c8 = c4.gatherNodesWithinHops(2, (n: Node) => ~~n.getId().substr(1) !== 2, false, true);
			expect(c8.count()).to.equal(1);
			let c9 = c4.gatherNodesWithinHops(2, (n: Node) => ~~n.getId().substr(1) !== 2, false, false);
			expect(c9.count()).to.equal(3);
		});

		it('can filter collections', () => {
			let m1 = new Map();
			m1.set('id', 'i1');
			n2.addData(m1);
			let n2f = graph.filter((e) => {
				return e.getAttribute('id') === 'i1';
			});
			expect(n2f.count()).to.equal(1);
			expect(n2f.getFirst()).to.equal(n2);
		});

		it('can destroy nodes', () => {
			let all = graph.getCollection();
			expect(all.count()).to.equal(7);
			n2.destroy();
			expect(n2.isDestroyed()).to.be.true;
			let cleared = all.clearDestroyed();
			expect(cleared).to.equal(1);
			expect(all.count()).to.equal(6);
			let l1 = n1.getLinks();
			expect(l1.count()).to.equal(1);
			all.clear();
			expect(all.getFirst()).to.equal(null);
		});

	})

	describe('saving and loading', () => {

		describe('random test', () => {
			let saved: string;
			let n = 50;
			let nodeCount: number, edgeCount: number;

			it('should save', async() => {
				let net = getTestNetRandom(n);
				nodeCount = net.getCollection().count();
				edgeCount = net.getCollection().getConnectionsMatching(true).count();
				let wr = new TestWritable();
				await net.saveToFile(wr);
				debug(wr.buffer.split("\n"));
				expect(wr.buffer.split("\n").length).to.greaterThan(n*2);
				saved = wr.buffer;
			});

			it('should load', async() => {
				let re = new TestReadable();
				re.buffer = saved;
				let net = new Graph();
				let stats = await net.loadFromFile(re);
				debug(stats);
				expect(stats.nodes).to.equal(nodeCount);
				expect(stats.edges).to.equal(edgeCount);
			});
		});

		describe('small test', () => {
			let saved: string;

			it('should save', async() => {
				let net = getTestNet1();
				let wr = new TestWritable();
				await net.saveToFile(wr);
				saved = wr.buffer;
			})

			it('should load', async() => {
				let net = new Graph();
				let re = new TestReadable();
				re.buffer = saved;
				let stats = await net.loadFromFile(re);
				debug(stats);
				let c1 = net.filter(e => {
					return e.getAttribute('a') === 1;
				});
				expect(c1.count()).to.equal(1);
				let n1 = c1.getFirst();
				let l1 = n1.getLinks().getFirst();
				expect(l1.getAttribute('a')).to.equal(3);
				expect(l1.getFrom()).to.equal(n1);
				let n2 = l1.getTo();
				expect(n2.getAttribute('a')).to.equal(2);
			});

		});



	});

});
