import Queue from '../../src/lib/queue';
import { expect } from 'chai';

describe('Queue', () => {

	it('can push and shift', () => {
		let q = new Queue<string>();
		q.push('a');
		q.push('b');
		expect(q.shift()).to.equal('a');
		expect(q.shift()).to.equal('b');
		expect(q.shift()).to.equal(null);
	});

	it('can unshift and pop', () => {
		let q = new Queue<number>();
		q.unshift(1);
		q.unshift(2);
		q.unshift(3);
		expect(q.shift()).to.equal(3);
		expect(q.pop()).to.equal(1);
		expect(q.pop()).to.equal(2);
		expect(q.pop()).to.equal(null);
	})

	it('can get empty repeatedly', () => {
		let q = new Queue<number>();
		for (let i=0; i<5; i++) {
			q.push(5);
			expect(q.shift()).to.equal(5);
			expect(q.shift()).to.equal(null);
		}
	});

});
